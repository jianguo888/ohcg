##  5分钟快速为OpenHarmony提交PR（Web）



活动标题：**【OpenHarmony开源贡献者计划2022】**

#### [哔哩哔哩视频教程](https://www.bilibili.com/video/BV17F411w7yE?spm_id_from=333.999.0.0&vd_source=7e8ad4a12f30d3f75d8dee50391800ae)

##  1. 简介

本文将讲述如何通过网页操作，3分钟内快速提交PR，适合于简单文档类修改PR。

整体流程为：

发现 Issue->创建Issue->[签署dco协议](https://dco.openharmony.cn/sign)->注册Gitee->fork仓库 -> 在线编辑（并添加扩展信息）-> 提交PR -> PR检测 -> 等待Committer审核->合入。



## 1.创建Issue

1.点击新建Issue

![image-20220811084233869](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220811084233869.png)

2.跳转如图所示界面，输入标题和内容



![image-20220811083950588](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220811083950588.png)

3.点击创建之后如图所示，记住#I5LPU2，后面关联Pr用到

![image-20220811084131549](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220811084131549.png)



## 2.签署Dco协议

[签署dco协议](https://dco.openharmony.cn/sign)

## 3.注册Gitee

[注册](https://gitee.com/jianguo888/)

## 4.Fork仓库



首先找到自己想提交PR的仓库，执行fork操作，把仓库fork到我的仓库。


1.如下图所示，我们想修改的仓库名称为docs，点击右上角fork按钮。

![image-20220811083151239](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220811083151239.png)

2.fork成功会自动进入自己fork的仓库，如下图所示。

![image-20220811083222897](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220811083222897.png)

##  5.修改内容

仓库已经fork完成后，我们通过Web在线修改内容，以修改README.md为例：

1.点击README.md打开该文件。

![image-20220811083309035](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220811083309035.png)

2.点击编辑，对该文件进行修改。

![image-20220811083327730](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220811083327730.png)

3.进入文件修改界面之后，文件内容为markdown语法格式修改内容。



4.点击小眼睛，预览修改之后的效果

![](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220811083415141.png)

5.文件修改完成之后，输入扩展信息，否则会导致PR无法通过DCO校验，格式为如下所示：

```
Signed-off-by: gitee用户名 <gitee绑定邮箱>
```

比如我的

```
Signed-off-by: 坚果 <852851198@qq.com>
```

输入扩展信息后如下图所示，点击提交，本地仓库修改完成。

![image-20220811083458493](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220811083458493.png)

## 6.提交PR

1. 点击左上角 docsl进入我们fork的本地仓库。

   ![image-20220811083536421](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220811083536421.png)

2. 点击Pull Request按钮进入提交PR页面。

   ![image-20220811083554737](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220811083554737.png)

3. 输入提交PR的标题，内容可以自己定义，并把#I5LPU2也添加到这然后点击创建 Pull Request。

   ![image-20220811084534808](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220811084534808.png)

4. 创建完成之后会自动跳转到我们的PR页面。



##  7.PR检测

PR提交完成后，需要对提交的PR进行检测是否符合仓库规则，检测成功后PR方可合并，一次完成的PR流程才算成功。

1. 在PR下方评论  start build 进行数据检测，触发门禁检查。

   ![image-20220811084710988](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220811084710988.png)

2. 刷新页面，查看检查结果。当出现**dco检查成功、代码质量检测成功、代码合规检测成功**后，就可以等待仓库管理员的合入啦！

到此，整个流程也就算是走完了，相比较本地操作的方式，这种更加快捷，但是问题也有，就是这只能用于修改简单的文字性描述，会比较合适，如果是代码层的，最好还是按照我提供的另一种方式来，会比较好。