# openharmony萌新贡献指南

[TOC]

本篇教程旨在帮助萌新们一起参与到OpenHarmony的开源事业中，让每个人成为开源人，星星之火，可以燎原！最下面有我的入群方式。

在这之前先说一下**，[从我这儿报名，提交Pr，](联系我.md)**

### [5分钟快速为OpenHarmony提交PR（Web）视频教程](five.md)



### [如何参与](联系我.md)

活动范围涵盖OpenHarmony主干仓、SIG仓、三方库，共计1000+个代码仓：

- OpenHarmony主库组织地址：

https://gitee.com/openharmony

- OpenHarmony SIG组织地址：

https://gitee.com/openharmony-sig

-  OpenHarmony三方库组织地址：

https://gitee.com/openharmony-tpc

## 前提条件

1. 有一个[Gitee账号](https://gitee.com/)，没有的话，记得[用邮箱注册一个](https://gitee.com/signup)
2. [签署开发者原创声明](https://dco.openharmony.cn/sign)
3. [本地安装Git](https://git-scm.com/downloads)

如果Git基础薄弱，也是可以用图形化界面

### windows推荐

- [tortoisegit](https://tortoisegit.org/)  
-  [sourcetree]( https://www.sourcetreeapp.com/) 

### mac推荐

-   [sourcetree]( https://www.sourcetreeapp.com/) 

接下来就是手把手的环节了

这儿也有两个图片可以看一下

- [FAQ](FAQ.md)
- [十分钟极速提Pr](十分钟极速提Pr.md)

## 一、配置Git

[注册码云账号](注册码云账号.md)

[本地安装Git](https://git-scm.com/downloads)

### 1.注册

**地址: https://gitee.com/
注册码云账号，只要点击导航条中的“注册”，或者点击首页中那个大大的“加入码云”按钮，即可进入注册页面。**

![image-20220719090715929](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220719090715929.png)

**输入账号、邮箱、密码,然后点击注册按钮.**

注册的时候最好取一个有意义的名字，比如姓名全拼，昵称全拼，如果被占用，可以加上有意义的数字.[比如我的](https://gitee.com/jianguo888)

注册完官方会向大家的邮箱发送一份激活邮件，请点击其中的链接激活账号，账号激活后，注册流程就算完成了。注册完毕即以新注册的账号登录，登录后即进入用户的控制面板页面。

找不到ssh-keygen命令是因为你的工作目录不在ssh-keygen.exe所在目录下，导致找不到命令，所以切换工作目录到ssh-kengen所在目录(Git/usr/bin/)即可。以我为例，我的Git安装在D盘Git下，所以进行操作 cd D:/Git/usr/bin/ ，然后执行 ssh-keygen -t rsa -C “您的邮箱地址” 即可

### 2.公钥认证管理

开发者向码云版本库写入最常用到的协议是 SSH 协议，因为 SSH 协议使用公钥认证，可以实现无口令访问，而若使用 HTTPS 协议每次身份认证时都需要提供口令。使用 SSH 公钥认证，就涉及到公钥的管理。

### 3.如何生成ssh公钥

------

你可以按如下命令来生成sshkey: 

这个邮箱就是你的上面的邮箱

```sh
ssh-keygen -t rsa -C "xxxxx@xxxxx.com"  

# Generating public/private rsa key pair...
# 三次回车即可生成 ssh key
```

比如我的

```
ssh-keygen -t rsa -C "852851198@qq.com"  
```

然后三次回车即可生成 ssh key，

查看你的 public key，

#### mac

```
cat ~/.ssh/id_rsa.pub
# ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6eNtGpNGwstc....
```

#### windows

在C:\Users\用户.ssh目录下找到id_rsa.pub复制里面所有内容

![image-20220719111429271](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220719111429271.png)



### 4.添加public key到码云

并把他添加到码云（Gitee.com） [SSH key添加地址](https://gitee.com/profile/sshkeys)

![image-20220719110915806](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220719110915806.png)

添加后，在终端（Terminal）中输入

```
ssh -T git@gitee.com
```

若返回

```
Welcome to Gitee.com, yourname!
```

则证明添加成功。

## 二、DCO签署

### 1.DCO签署网址

[开发者原创声明Developer Certificate of Origin](https://dco.openharmony.cn/sign)

[开发者原创声明](https://dco.openharmony.io/sign-dco)

### 2.签署DCO

> 注意：
>
> DCO签署Name 必须要和git config --global user.name 设置保持一致（其实不一致也可以，但一致之后少麻烦）
>
> DCO签署E-mail必须要和git config --global user.email设置保持一致



![向OpenHarmony社区提交代码](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220625103528468.png)

### 3.修改本地的邮箱和签署邮箱一致

```
git config --global user.name "你的名字" 
git config --global user.email "你的gitee绑定邮箱"
git config --global  --list
```

比如我的：

```html
git config --global user.name "徐建国" 
git config --global user.email "852851198@qq.com"
git config --global  --list
```



## 三、提交代码

### 1.下载代码

#### 1.1找到要提交代码的社区代码仓，fork一个到自己的私有仓中；

比如我fork的是docs仓

原地址：https://gitee.com/openharmony/docs

fork后：https://gitee.com/jianguo888/docs

而你后面要git clone的就是后者。



![image-20220625103805814](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220625103805814.png)



#### 1.2复制自己私有仓的“克隆/下载”中的HTTPS/SSH链接下载代码

优先ssh

本地创建工作区，然后

```
git clone git@gitee.com:你的giteeID/docs.git
```

比如我的

```
git clone git@gitee.com:jianguo888/docs.git
```

![image-20220805090643509](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220805090643509.png)

### 2.提交代码

#### 2.1将修改的代码合入到提交代码仓中；

必须添加邮箱，否则后面的doc校验不过

切记：提交的时候必须是-s -m

 -s的意思就是带了你的signoff

```
git add .
git commit -s -m '修改信息' // 提交信息包括你的概要信息
git push -f origin master 

```

比如我的：

```
git add .
git commit -s -m '修改redeme提高可读性 ' // 提交信息包含signoff邮箱
git push -f origin master 


```

####  2.2如何追加signoff到上一次commit?

执行`git commit --amend --signoff`命令 。

#### 2.3如果是对同一个问题修改

```
git commit --amend
```

通常推荐一个一个commit解决一个问题

## 四、提交ISSUE

### 1.[进入社区主代码建Issue](https://gitee.com/openharmony/docs)（不是fork的代码仓），同时注意建Issue有很多选项类型可选择，根据实际情况选择

名称

【OpenHarmony开源贡献者计划2022】+Issue内容描述

![image-20220625105723070](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220625105723070.png)



![向OpenHarmony社区提交代码-开源基础软件社区](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/resize,w_617,h_277.png)

### 2.创建成功会生成一个#XXXXX(I5E2H2)的IssueID，后续提交PR可以关联，关联PR合入，Issue就会自动关闭。

![image-20220625105939890](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220625105939890.png)

## 五、提交PR

### 1.代码提交到自己的私有仓，刷新，点击“+ Pull Request”建PR合入代码到社区主代码仓；

![image-20220625110150526](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220625110150526.png)

### 2.进入PR提交界面，可选择代码仓库分支，和关联ISSUE ID，简单描述合入的PR修改等信息；

![image-20220625110210970](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220625110210970.png)

标题都是【OpenHarmony开源贡献者计划2022】+你修改的任务摘要

比如我的：

【OpenHarmony开源贡献者计划2022】+智能家居中控



![image-20220719131555042](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220719131555042.png)

#### 关联ISSUE ID

第一步查看自己的issues ID

![image-20220723140925451](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220723140925451.png)

第二步添加到自己的Pull request的描述里

![image-20220723141445018](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220723141445018.png)

### 3.PR建立成功，首先默认进行DCO检查，检查成功，需要手动在评论区输入回复”start build”方可进入代码的CI静态检查和编译等操作。

![image-20220625110230911](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220625110230911.png)


## 六、联系committer

committer：https://gitee.com/openharmony/community/blob/master/zh/committer.md

### 1. committer文档中找到对应的committer负责人主页，想办法联系

这一步，可以找我沟通就好，我帮大家联系。

我是在committer发现负责人，然后微信搜索，群里捞到的。

![向OpenHarmony社区提交代码](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/resize,w_632,h_283.png)



![image-20220625110507371](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220625110507371.png)



## 七、我的群以及微信

### 个人微信

![image-20220719100136901](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220719100136901.png)

### 加群


加我微信，我拉群
## [仓颉语言内测申请](https://wj.qq.com/s2/9931522/fed7/)

## 参考资料

[贡献代码的流程](https://gitee.com/openharmony/docs/blob/master/zh-cn/contribute/%E8%B4%A1%E7%8C%AE%E4%BB%A3%E7%A0%81.md)

[贡献流程](https://gitee.com/openharmony/docs/blob/master/zh-cn/contribute/贡献流程.md)

## 坚果的小伙伴们提的Pr

**悠悠森**:https://gitee.com/openharmony/docs/pulls/6910

https://gitee.com/openharmony/docs/pulls/6921

https://gitee.com/openharmony/docs/pulls/6937

https://gitee.com/openharmony-sig/knowledge_demo_entainment/pulls/35

https://gitee.com/openharmony-sig/knowledge_demo_travel/issues/I5IKVK
