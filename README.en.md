# openharmony萌新贡献指南





待认领issue：

https://gitee.com/openharmony/docs/issues/I5IKF9?from=project-issue



活动细节：

https://docs.qq.com/doc/DQllxQm1hU1RnTWlJ





如果是Pr已经合并了，然后又发现了新的需要需要的，可以这样操作

![image-20220729084657272](../../Library/Application Support/typora-user-images/image-20220729084657272.png)



点击这个按钮，便会将所有的主干的代码更新，这个时候你的远程仓库，所有的内容就会与OpenHarmony/docs仓保持一致，



![image-20220729084736313](../../Library/Application Support/typora-user-images/image-20220729084736313.png)



随后在本地打开控制台

输入：

```
git pull origin master
```

这个时候远程和本地一致了，之后，就可以继续你的修改了，这样的好处是防止你的修改出现冲突。

